/**
 * @jsx React.DOM
 */

var React = require('react');
var Layout = require('./layout');

module.exports = React.createClass({displayName: 'exports',
	render: function() {
		return (
			Layout(null, 
				React.DOM.div(null, 
					React.DOM.h1(null, message),
					React.DOM.h2(null, error.status),
					React.DOM.pre(null, error.stack)
				)
			)
		);
	}
});

