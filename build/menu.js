/**
 * @jsx React.DOM
 */

var React = require('react');
var Layout = require('./layout');

module.exports = React.createClass({displayName: 'exports',
	render: function() {
		return (
			Layout(null, 
				React.DOM.div( {className:"content"}, 
					React.DOM.div(null, 
						React.DOM.h1(null, 
							React.DOM.a( {href:"/", className:"right"}, "Logout"),
							"Shared Menu"
						),
						React.DOM.div( {className:"menu-container"}, 
							React.DOM.div( {id:"menu"})
						),
						React.DOM.div( {className:"info"}),
						React.DOM.div( {id:"edit_root", className:"templates"}, 
							React.DOM.h2(null, "Edit Root Node"),
							React.DOM.p(null, "Name: ", React.DOM.input( {type:"text", id:"node_name"} )),
							React.DOM.p(null, React.DOM.button( {type:"button", id:"update"}, "Update Node")),
							React.DOM.h2(null, "Create a new factory"),
							React.DOM.p(null, "Name: ", React.DOM.input( {type:"text", id:"factory_name"} )),
							React.DOM.p(null, "Range: ", React.DOM.input( {type:"number", id:"factory_low", size:"4"} ), " - ", React.DOM.input( {type:"number", id:"factory_high", size:"4"} )),
							React.DOM.p(null, React.DOM.button( {type:"button", id:"create"}, "Create Factory"))
						),
						React.DOM.div( {id:"edit_factory", class:"templates"}, 
							React.DOM.h2(null, "Edit Factory"),
							React.DOM.p(null, "Name: ", React.DOM.input( {type:"text", id:"factory_name"} )),
							React.DOM.p(null, "Range: ", React.DOM.input( {type:"number", id:"factory_low", size:"4"} ), " - ", React.DOM.input( {type:"number", id:"factory_high", size:"4"} )),
							React.DOM.p(null, 
								React.DOM.button( {type:"button"}, "Update Factory"),
								React.DOM.button( {type:"button"}, "Remove Factory")
							)
						),
						React.DOM.div( {id:"show_node", className:"templates"}, 
							React.DOM.h2(null, "Node"),
							React.DOM.p(null, "This year in:"),
							React.DOM.ul(null, 
								React.DOM.li(null, "18XX: asdf"),React.DOM.li(null, "19XX: asdf"),React.DOM.li(null, "20XX: asdf")
							)
						)
					)
				)
			)
		);
	}
});

