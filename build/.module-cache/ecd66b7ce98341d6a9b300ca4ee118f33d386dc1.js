/** @jsx React.DOM */

var React = require('react');

module.exports = React.createClass({displayName: 'exports',
	render: function() {
		return (
			React.DOM.html(null, 
				React.DOM.head(null, 
					React.DOM.title(null, "Shared Menu"),
					React.DOM.link( {rel:"stylesheet", type:"text/css", href:"/stylesheets/style.css"} ),
					React.DOM.link( {rel:"stylesheet", type:"text/css", href:"/stylesheets/style.min.css"} ),
					React.DOM.script( {type:"text/javascript", src:"//code.jquery.com/jquery-2.1.0.min.js"}),
					React.DOM.script( {type:"text/javascript", src:"/javascripts/jstree.min.js"}),
					React.DOM.script( {type:"text/javascript", src:"/javascripts/socket.io.min.js"}),
					React.DOM.script( {type:"text/javascript", src:"/javascripts/bundle.js"})
				),
				React.DOM.body(null, 
					React.DOM.div( {className:"container-outer"}, 
						React.DOM.div( {className:"container-inner"}, 
							React.DOM.div( {className:"content"}
							)
						)
					)
				)
			)
		);
	}
});

