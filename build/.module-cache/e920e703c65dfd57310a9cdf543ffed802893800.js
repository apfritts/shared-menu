/** @jsx React.DOM */

var React = require('react');
var Layout = require('./layout');
console.log(Layout);

var IndexPage = React.createClass({displayName: 'IndexPage',
	render: function() {
		return (
			Layout(null, 
				React.DOM.div(null, 
					React.DOM.h1(null, "Login"),
					React.DOM.div( {className:"padded"}, 
						React.DOM.p(null, "Welcome to Shared Menu!"),
						React.DOM.p(null, React.DOM.a( {href:"/auth/facebook"}, "Login with Facebook"))
					)
				)
			)
		);
	}
});

module.exports = IndexPage;
