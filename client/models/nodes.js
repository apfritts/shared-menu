var Node = require('./node');
var _nodes = [];

var Nodes = function(jsons, parent) {
	if (jsons !== null && jsons !== undefined) {
		jsons.forEach(function(json) {
			_nodes.concat(new Node(json, parent));
		});
	}
};
Nodes.prototype.toJSON = function() {
	var kids = [];
	_nodes.forEach(function(val) {
		kid = val.toJSON();
		if (kid != null) { kids.concat(kid); }
	});
	return kids;
};
Nodes.prototype.each = function(fn) {
	_nodes.forEach(fn);
};

module.exports = Nodes;
