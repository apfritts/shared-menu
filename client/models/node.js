var Nodes = require('./nodes');
var _id = null, _text = null, _low = null, _high = null, _parent = null, _children = [];

var Node = function(json, parent) {
	_id       = json.id;
	_text     = json.text;
	_low      = json.low;
	_high     = json.high;
	_parent   = parent;
	_children = new Nodes(json.children, this);
};
Node.prototype.toJSON = function() {
	if (_text == null) { return null; }
	var kids = [];
	_children.each(function(val) {
		kid = val.toJSON();
		if (kid != null) { kids.concat(kid); }
	});
	return { id: _id, text: _text, data: {low: _low, high: _high}, children: _children, parent: _parent};
};
Node.prototype.setId = function(id) {
	_id = id;
};
Node.prototype.getId = function() {
	return _id;
};
Node.prototype.update = function(text) {
	_text = text;
};
Node.prototype.remove = function() {
	_text = null;
	children = [];
};

module.exports = Node;
