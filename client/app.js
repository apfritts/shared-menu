var Node  = require('./models/node');
var Nodes = require('./models/nodes');
var jstree = null;

$(function() {
	if ($('.info').length == 0) {
		return;
	}
	var socket = io.connect('/');
	var selectedNode = null;
	var validateFactory = function() {
		var name = $('#factory_name', '.info').val();
		var low = $('#factory_low', '.info').val();
		var high = $('#factory_high', '.info').val();
		if (name.length == 0) { alert('Please enter a factory name!'); $('#factory_name', '.info').focus(); return false; }
		if (low.length == 0 || isNaN(parseInt(low))) { alert('Please enter a low range integer!'); $('#factory_low', '.info').focus(); return false; }
		if (high.length == 0 || isNaN(parseInt(high))) { alert('Please enter a high range integer!'); $('#factory_high', '.info').focus(); return false; }
		low = parseInt(low);
		high = parseInt(high);
		if (low >= high) { alert('You need to increase your high number. Or reduce your low number.' + low + ":" + high); $('#factory_high', '.info').focus(); return false; }
		return [name, low, high];
	};
	var factoryCreate = function(e) {
		e.preventDefault();
		e.stopPropagation();
		var fields = validateFactory();
		if (fields == false) { return false; }
		var node = new Node({text: fields[0], low: fields[1], high: fields[2]}, selectedNode.id);
		node.setId(jstree.create_node(selectedNode, node.toJSON()));
		jstree.open_node(selectedNode);
		jstree.deselect_node(selectedNode);
		jstree.select_node(node.getId());
		socket.emit('create', node.toJSON());
	};
	var factoryUpdate = function(e) {
		e.preventDefault();
		e.stopPropagation();
		var fields = validateFactory();
		if (fields == false) { return false; }
		selectedNode.text = fields[0];
		selectedNode.data.low = fields[1];
		selectedNode.data.high = fields[2];
		jstree.redraw(true);
		var node = new Node({id: selectedNode.id, text: fields[0], low: fields[1], high: fields[2]}, selectedNode.parent);
		socket.emit('update', node.toJSON());
	};
	var factoryRemove = function(e) {
		e.preventDefault();
		e.stopPropagation();
		var node = new Node({id: selectedNode.id, text: selectedNode.text, low: selectedNode.data.low, high: selectedNode.data.high}, selectedNode.parent);
		jstree.delete_node(selectedNode);
		selectedNode = null;
		showAndConnectView('home');
		socket.emit('remove', node.toJSON());
	};
	var factoryGenerate = function(e) {
		e.preventDefault();
		e.stopPropagation();
		var number = $('#number', '.info').val();
		if (number.length == 0 || isNaN(parseInt(number))) { alert('Hmm...I only understand base 10 numbers. Try again!'); $('#number', '.info').focus(); return false; }
		number = parseInt(number);
		if (number < 1 || number > 15) { alert('Oops! You need a number in the range 1 <= number <= 15.'); $('#number', '.info').focus(); return false; }
		// clear the sub nodes
		var kids = jstree.get_node(selectedNode).children;
		while (kids.length > 0) {
			jstree.delete_node(kids);
			kids = jstree.get_node(selectedNode).children;
		}
		// generate the new numbers
		var nums = {};
		for (var i = 0; i < number; i++) {
			var random = (Math.floor(Math.random() * (selectedNode.data.high - selectedNode.data.low + 1)) + selectedNode.data.low).toString();
			console.log(selectedNode);
			console.log(random);
			var id = jstree.create_node(selectedNode, {text: random});
			nums[id] = random;
		}
		jstree.open_node(selectedNode);
		socket.emit('generate', {parent: selectedNode.id, numbers: nums});
	};
	var templates = {
		home: {t: $('.templates#home'), a: {}},
		factory_create: {
			t: $('.templates#factory_create'),
			a: {
				'#create': factoryCreate
			}
		},
		edit_factory: {
			t: $('.templates#edit_factory'),
			a: {
				'#update': factoryUpdate,
				'#generate': function() { showAndConnectView('generator'); },
				'#remove': factoryRemove
			}
		},
		generator: {t: $('.templates#generator'), a: {'#generate': factoryGenerate}},
		show_node: {t: $('.templates#show_node'), a: {}},
		error: {t: $('.templates#error'), a: {}}
	};
	function showAndConnectView(view) {
		$('.info', 'html').html(templates[view]['t'].html());
		console.log(view);
		for(var a in templates[view]['a']) {
			$(a, '.info').on('click', templates[view]['a'][a]);
		}
	}
	showAndConnectView('home');
	$.ajax('/data', {
			success: function(data, status, xhr) {
				// Create the tree
				jstree = $.jstree.create('div#menu', {core: {data: data, check_callback: true, error: function() { console.log(arguments);}, multiple: false}});
				// Connect up the context menu right click
				$.contextMenu({
					selector: 'div#menu > ul > li > ul > li',
					callback: function(key, options) {
						selectedNode = jstree.get_node(this);
						showAndConnectView('generator');
					},
					items: {
						generate: {name: 'Generate'}
					}
				});
			},
			error: function(xhr, status, error) { showAndConnectView('error'); }
	});

	$('div#menu').on('changed.jstree', function (e, data) {
		console.log(e);
		console.log(data);
		selectedNode = data.node;
		if (data.node.parent == '#') {
			showAndConnectView('factory_create');
		} else {
			if (data.node.data == null) {
				showAndConnectView('show_node');
			} else {
				showAndConnectView('edit_factory');
				$('#factory_name', '.info').val(data.node.text);
				$('#factory_low',  '.info').val(data.node.data.low);
				$('#factory_high', '.info').val(data.node.data.high);
			}
		}
	});
	$('div#menu li', '.info').on('contextmenu', function(e, data) {
		e.preventDefault();
		e.stopPropagation();
		var el = $(e.currentTarget);
		var node = jstree.get_node(el);
		showAndConnectView('show_node');
	});
	socket.on('create', function(data) {
		jstree.create_node(data.parent, data);
	});
	socket.on('update', function(data) {
		var node = jstree.get_node(data.id);
		node.text = data.text;
		node.data.low = data.data.low;
		node.data.high = data.data.high;
		jstree.redraw(true);
		if (node == selectedNode) {
			showAndConnectView('edit_factory');
			$('#factory_name', '.info').val(data.text);
			$('#factory_low',  '.info').val(data.data.low);
			$('#factory_high', '.info').val(data.data.high);
		}
	});
	socket.on('remove', function(data) {
		var del_me = jstree.get_node(data.id);
		jstree.delete_node(del_me);
		if (selectedNode.id == del_me.id) {
			selectedNode = null;
			showAndConnectView('home');
		 }
	});
	socket.on('generate', function(data) {
		var topNode = jstree.get_node(data.parent);
		// clear the sub nodes
		var kids = jstree.get_node(topNode).children;
		while (kids.length > 0) {
			jstree.delete_node(kids);
			kids = jstree.get_node(topNode).children;
		}
		// Add the new numbers
		for (var id in data.numbers) {
			jstree.create_node(topNode, {id: id, text: data.numbers[id]});
		}
	});
	socket.on('error', function(data) {
		alert(data.message);
		location.href = '/menu';
	});
});

window.jstree = function() { return jstree; }

