var debug             = require('debug')('my-application');
var express           = require('express');
var path              = require('path');
var favicon           = require('static-favicon');
var logger            = require('morgan');
var cookieParser      = require('cookie-parser');
var cookieSession     = require('cookie-session');
var bodyParser        = require('body-parser');
var passport          = require('passport');
var passport_facebook = require('passport-facebook').Strategy;
var socketio          = require('socket.io');
var mongo             = require('mongodb').MongoClient
var format            = require('util').format;
var browserify        = require('browserify');

// We need JSX transformations!
require('node-jsx').install();

// Load our routers
var routes = require('./routes/index');
var menu = require('./routes/menu');

// Create the app!
var app = express();

// Middleware
app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(cookieSession({ secret: 'asdf' }));
app.use(express.static(path.join(__dirname, 'public')));

// Data persistance
var db = null, storage = null, tree = null, indexes = null;
var saveTree = function() {
	storage.update({type: 'tree'}, tree, {upsert: true}, function(err) { if (err) throw err; });
};
mongo.connect('mongodb://127.0.0.1:27017/shared_menu', function(err, _db) {
	if (err) throw err;
	db = _db;
	storage = db.collection('datas');
	storage.find({type: 'tree'}).toArray(function(err, results) {
		if (results.length == 0) {
			tree = {root: [], type: 'tree'};
			indexes = {};
		} else {
			tree = results[0];
			indexes = {};

			// create indexes
			tree.root.forEach(function(root) {
				indexes[root.id] = root;
				root.children.forEach(function(factory) {
					indexes[factory.id] = factory;
				});
			});
		}
	});
});

var createRoot = function(user) {
	tree.root[tree.root.length] = {
		text: user.text,
		id: user.facebook_id,
		children: []
	};
	indexes[user.facebook_id] = tree.root[tree.root.length - 1];
	storage.findAndModify(
		{type: 'tree'},
		null,
		tree,
		null,
		function(err, tree) {
			if (err) {
				console.error(err);
			} else {
				saveTree();
				broadcastUser(user);
			}
		}
	);
};

// Login configuration
var development = app.get('env') === 'development';
passport.use(new passport_facebook(
	{
		clientID: '671103622926356',
		clientSecret: 'a000d4c8bf271124425295351ae5ef56',
		callbackURL: 'http://' + (development ? '127.0.0.1:3000' : 'pp.apfritts.com') + '/auth/facebook/callback'
	},
	function(accessToken, refreshToken, profile, done) {
		var user_obj = {text: profile.name.givenName + ' ' + profile.name.familyName, facebook_id: profile.id, type: 'user'};
		storage.findAndModify(
			{facebook_id: profile.id, type: 'user'},
			null,
			user_obj,
			{new: true, upsert: true},
			function(err, user) {
				if (err) {
					console.warn(err);
					throw err;
				}
				// Make sure they are in the tree!
				if (indexes[user.facebook_id] == null) {
					createRoot(user);
				}
				done(null, user);
			}
		);
	}
));
passport.serializeUser(function(user, done){
	done(null, user._id);
});
passport.deserializeUser(function(id, done){
	done(null, storage.find({_id: id, type: 'user'}));
});
app.use(passport.initialize());
app.use(passport.session());
app.get('/auth/facebook', passport.authenticate('facebook'));
app.get('/auth/facebook/callback', passport.authenticate('facebook', { successRedirect: '/menu', failureRedirect: '/', failureFlash: true }));

app.use('/', routes);
app.use('/menu', menu);

app.get('/data', function(req, res, next) {
	res.send(tree.root);
});

if (development) {
	app.use('/javascripts/bundle.js', function(req, res, next) {
		browserify(
			'./client/app.js',
			{debug: development}
		).bundle(function(err, src) {
			if (err) throw err;
			res.send(src);
		});
	});
}

// Catch 404 and forwarding to error handler
app.use(function(req, res, next) {
		var err = new Error('Not Found');
		err.status = 404;
		next(err);
});

// 500 error with stack trace if appropriate
app.use(function(err, req, res, next) {
	var status = err.status || 500;
	res.status(status);
	res.send(status, {
		message: err.message,
		error: (development ? err : {})
	});
});

app.set('port', process.env.PORT || 3000);
var server = app.listen(app.get('port'), function() {
	console.log('Express server listening on port ' + server.address().port + ' in ' + (development ? 'development' : 'production') + ' mode.');
});

// Enable constant communication
io = socketio.listen(server);
io.sockets.on('connection', function(socket) {
	socket.on('create', function(data) {
		if (indexes[data.id]) {
			socket.emit('error', {message: 'Hmm..that factory ID already exists.'});
		} else {
			if (indexes[data.parent]) {
				indexes[data.parent].children.push(data);
				indexes[data.id] = data;
				saveTree();
				socket.broadcast.emit('create', data);
			} else {
				socket.emit('error', {message: 'Ooops! Not sure where you got that root node.'});
			}
		}
	});
	socket.on('update', function(data) {
		if (indexes[data.id]) {
			indexes[data.id].text = data.text;
			indexes[data.id].data = data.data;
			saveTree();
			socket.broadcast.emit('update', data);
		} else {
			socket.emit('error', {message: 'Cannot find that factory.'});
		}
	});
	socket.on('remove', function(data) {
		if (indexes[data.id]) {
			var parent_id = indexes[data.id].parent;
			var new_kids = [];
			for (var i in indexes[parent_id].children) {
				if (indexes[parent_id].children[i].id != data.id) {
					new_kids.push(indexes[parent_id].children[i]);
				}
			}
			indexes[parent_id].children = new_kids;
			delete(indexes[data.id]);
			saveTree();
			socket.broadcast.emit('remove', data);
		} else {
			socket.emit('error', {message: 'Yikes! I can\'t delete something I know nothing about.'});
		}
	});
	socket.on('generate', function(data) {
		if (indexes[data.parent]) {
			indexes[data.parent].children = [];
			for (var id in data.numbers) {
				indexes[data.parent].children.push({id: id, text: data.numbers[id]});
			}
			saveTree();
			socket.broadcast.emit('generate', data);
		} else {
			socket.emit('error', {message: 'Unable to find factory.'});
		}
	});
});

function broadcastUser(user) {
	io.sockets.emit('create', {text: user.text, parent: null, children: [], id: user.facebook_id});
}
