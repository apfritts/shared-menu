/** @jsx React.DOM */

var React = require('react');
var Layout = require('./layout');

var IndexPage = React.createClass({
	render: function() {
		return (
			<div>
				<h1>Login</h1>
				<div className="padded">
					<p>Welcome to Shared Menu!</p>
					<p><a href="/auth/facebook">Login with Facebook</a></p>
				</div>
			</div>
		);
	}
});

module.exports = IndexPage;
