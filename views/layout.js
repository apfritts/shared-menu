/** @jsx React.DOM */

var React      = require('react');

var Layout = React.createClass({
	page: null,
	setPage: function(page) {
		this.page = page;
	},
	render: function() {
		return (
			<html>
				<head>
					<title>Shared Menu</title>
					<meta name="viewport" content="initial-scale=1.0" />
					<link rel="stylesheet" type="text/css" href="/stylesheets/style.css" />
					<link rel="stylesheet" type="text/css" href="/stylesheets/style.min.css" />
					<link rel="stylesheet" type="text/css" href="/stylesheets/jquery.contextMenu.css" />
					<script type="text/javascript" src="/javascripts/jquery.js"></script>
					<script type="text/javascript" src="/javascripts/jquery.ui.position.js"></script>
					<script type="text/javascript" src="/javascripts/jquery.contextMenu.js"></script>
					<script type="text/javascript" src="/javascripts/jstree.js"></script>
					<script type="text/javascript" src="/javascripts/socket.io.min.js"></script>
					<script type="text/javascript" src="/javascripts/bundle.js"></script>
				</head>
				<body>
					<div className="container-outer">
						<div className="container-inner">
							<div className="content">
								{this.page.render()}
							</div>
						</div>
					</div>
				</body>
			</html>
		);
	}
});

module.exports = Layout;
