/**
 * @jsx React.DOM
 */

var React = require('react');

module.exports = React.createClass({
	render: function() {
		return (
			<div>
				<h1>{message}</h1>
				<h2>{error.status}</h2>
				<pre>{error.stack}</pre>
			</div>
		);
	}
});

