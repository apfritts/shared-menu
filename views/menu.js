/**
 * @jsx React.DOM
 */

var React = require('react');

module.exports = React.createClass({
	render: function() {
		return (
			<div className="content">
				<div>
					<h1>
						<a href="/" className="right">Logout</a>
						Shared Menu
					</h1>
					<div className="menu-container">
						<div id="menu"></div>
					</div>
					<div className="info"></div>
					<div id="home" className="templates">
						<h2>Howdy! And Welcome!</h2>
						<p>You should start by selecting a root node and creating a factory!</p>
					</div>
					<div id="factory_create" className="templates">
						<h2>Create a new factory</h2>
						<p>Name: <input type="text" id="factory_name" /></p>
						<p>Range: <input type="number" id="factory_low" size="2" /> - <input type="number" id="factory_high" size="2" /></p>
						<p><button type="button" id="create">Create Factory</button></p>
					</div>
					<div id="edit_factory" className="templates">
						<h2>Edit Factory</h2>
						<p>Name: <input type="text" id="factory_name" /></p>
						<p>Range: <input type="number" id="factory_low" size="2" /> - <input type="number" id="factory_high" size="2" /></p>
						<p>
							<button type="button" id="update">Update</button>
							<button type="button" id="generate">Generate</button>
							<button type="button" id="remove">Remove</button>
						</p>
					</div>
					<div id="show_node" className="templates">
						<h2>Node</h2>
						<p>This year in:</p>
						<ul>
							<li>18XX: asdf</li><li>19XX: asdf</li><li>20XX: asdf</li>
						</ul>
					</div>
					<div id="generator" className="templates">
						<h2>Generator</h2>
						<p>How many numbers between <span id="low"></span> and <span id="high"></span> would you like to generate?</p>
						<p><input type="number" value="5" id="number" /></p>
						<p><button type="button" id="generate">Make it so!</button></p>
					</div>
					<div id="error" className="templates">
						<h2>Error</h2>
						<p>A communication error has occurred. Please try <a href="/menu">reloading this page</a>.</p>
					</div>
				</div>
			</div>
		);
	}
});

