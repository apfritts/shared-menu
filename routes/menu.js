var express = require('express');
var router = express.Router();

var React      = require('react');
var ReactAsync = require('react-async');
require('react/lib/ReactMount').allowFullPageRender = true;
var LayoutPage = require('../views/layout');

router.get('/', function(req, res) {
	var layout = new LayoutPage();
	layout.setPage(new require('../views/menu')());
	ReactAsync.renderComponentToStringWithAsyncState(layout, function(err, markup) {
		res.send(markup);
	});
});

module.exports = router;
